/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.google.appindexing.util;

import com.google.common.collect.Lists;

import com.intellij.openapi.module.Module;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiFile;
import com.intellij.psi.PsiManager;
import com.intellij.psi.XmlRecursiveElementVisitor;
import com.intellij.psi.xml.XmlTag;
import com.intellij.psi.xml.XmlFile;

import org.jetbrains.android.facet.AndroidFacet;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.File;
import java.util.List;

/**
 * Util functions for getting AndroidManifest.xml file or tags within the file.
 */
public final class ManifestUtils {
  /**
   * Returns the file of AndroidManifest.xml.
   */
  @Nullable
  public static VirtualFile getAndroidManiFest(@NotNull Module module) {
    AndroidFacet facet = AndroidFacet.getInstance(module);
    if (facet != null) {
      File file = facet.getMainSourceProvider().getManifestFile();
      if (file != null) {
        return LocalFileSystem.getInstance().findFileByIoFile(file);
      }
    }
    return null;
  }

  /**
   * Returns the file of AndroidManifest.xml with type of XmlFile.
   */
  @Nullable
  public static XmlFile getAndroidManifestPsi(@NotNull Module module) {
    VirtualFile manifest = getAndroidManiFest(module);
    if (manifest != null) {
      PsiFile psiFile = PsiManager.getInstance(module.getProject()).findFile(manifest);
      if (psiFile instanceof XmlFile) {
        return (XmlFile)psiFile;
      }
    }
    return null;
  }

  /**
   * Searches xml tags with a specific tag name within a root tag.
   *
   * @param root    The root tag to search in.
   * @param tagName The tag name.
   * @return All the xml tags with the name.
   */
  @NotNull
  public static List<XmlTag> searchXmlTagsByName(@NotNull XmlTag root, @NotNull final String tagName) {
    final List<XmlTag> tags = Lists.newArrayList();
    root.accept(new XmlRecursiveElementVisitor() {
      @Override
      public void visitXmlTag(XmlTag tag) {
        super.visitXmlTag(tag);
        if (tag.getName().equalsIgnoreCase(tagName)) {
          tags.add(tag);
        }
      }
    });
    return tags;
  }
}

