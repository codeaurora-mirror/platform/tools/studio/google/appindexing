package com.example.creation;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends Activity {

  private GoogleApiClient client;

  /**
   * ATTENTION: This was auto-generated to implement the App Indexing API.
   * See https://g.co/AppIndexing/AndroidStudio for more information.
   */
  private GoogleApiClient client3;

  @Override
  public void onStart() {
    super.onStart();
    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client3.connect();
    GoogleApiClient client2 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    client = client2;
    client.connect();
    Action viewAction = Action.newAction(
        Action.TYPE_VIEW,
        "Main Page",
        Uri.parse("http://www.example.com/main"),
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    AppIndex.AppIndexApi.start(client, viewAction);
    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    Action viewAction2 = Action.newAction(
        Action.TYPE_VIEW,// TODO: choose an action type.
        "Main Page",// TODO: Define a title for the content shown.
        // TODO: If you have web page content that matches this app activity's content,
        // make sure this auto-generated web page URL is correct.
        // Otherwise, set the URL to null.
        Uri.parse("http://www.example.com/main"),
        // TODO: Make sure this auto-generated app deep link URI is correct.
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    AppIndex.AppIndexApi.start(client3, viewAction2);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client3 = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
  }

  @Override
  public void onStop() {
    super.onStop();

    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    Action viewAction2 = Action.newAction(
        Action.TYPE_VIEW,// TODO: choose an action type.
        "Main Page",// TODO: Define a title for the content shown.
        // TODO: If you have web page content that matches this app activity's content,
        // make sure this auto-generated web page URL is correct.
        // Otherwise, set the URL to null.
        Uri.parse("http://www.example.com/main"),
        // TODO: Make sure this auto-generated app deep link URI is correct.
        Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    AppIndex.AppIndexApi.end(client3, viewAction2);
    client3.disconnect();
  }
}
