package com.example.creation;

import a.Action;
import a.AppIndex;
import a.GoogleApiClient;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

public class MainActivity extends Activity {

  /**
   * ATTENTION: This was auto-generated to implement the App Indexing API.
   * See https://g.co/AppIndexing/AndroidStudio for more information.
   */
  private com.google.android.gms.common.api.GoogleApiClient client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client = new com.google.android.gms.common.api.GoogleApiClient.Builder(this).addApi(com.google.android.gms.appindexing.AppIndex.API).build();
  }

  @Override
  public void onStart() {
    super.onStart();

    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    client.connect();
    com.google.android.gms.appindexing.Action viewAction = com.google.android.gms.appindexing.Action.newAction(
      com.google.android.gms.appindexing.Action.TYPE_VIEW,// TODO: choose an action type.
      "Main Page",// TODO: Define a title for the content shown.
      // TODO: If you have web page content that matches this app activity's content,
      // make sure this auto-generated web page URL is correct.
      // Otherwise, set the URL to null.
      Uri.parse("http://www.example.com/main"),
      // TODO: Make sure this auto-generated app deep link URI is correct.
      Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    com.google.android.gms.appindexing.AppIndex.AppIndexApi.start(client, viewAction);
  }

  @Override
  public void onStop() {
    super.onStop();

    // ATTENTION: This was auto-generated to implement the App Indexing API.
    // See https://g.co/AppIndexing/AndroidStudio for more information.
    com.google.android.gms.appindexing.Action viewAction = com.google.android.gms.appindexing.Action.newAction(
      com.google.android.gms.appindexing.Action.TYPE_VIEW,// TODO: choose an action type.
      "Main Page",// TODO: Define a title for the content shown.
      // TODO: If you have web page content that matches this app activity's content,
      // make sure this auto-generated web page URL is correct.
      // Otherwise, set the URL to null.
      Uri.parse("http://www.example.com/main"),
      // TODO: Make sure this auto-generated app deep link URI is correct.
      Uri.parse("android-app://com.example.creation/http/www.example.com/main")
    );
    com.google.android.gms.appindexing.AppIndex.AppIndexApi.end(client, viewAction);
    client.disconnect();
  }
}
