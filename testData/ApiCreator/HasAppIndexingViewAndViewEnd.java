package com.example.creation;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.common.api.GoogleApiClient;

public class MainActivity extends Activity {
  <caret>
  private GoogleApiClient client;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
  }

  @Override
  public void onStart() {
    super.onStart();
    client.connect();
    AppIndex.AppIndexApi.view(client, this,
        Uri.parse("android-app://com.example.creation/http/www.example.com/main"),
        "Main Page", Uri.parse("http://www.example.com/main"), null);
  }

  @Override
  public void onStop() {
    super.onStop();
    AppIndex.AppIndexApi.viewEnd(client, this,
        Uri.parse("android-app://com.example.creation/http/www.example.com/main"));
    client.disconnect();
  }
}
